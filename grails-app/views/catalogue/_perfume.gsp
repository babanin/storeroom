<div class="form-group">
  <label class="col-md-4 control-label">Name</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input name="name" placeholder="Name" class="form-control" type="text" value="${perfume ? perfume.name : ''}">
      <span class="input-group-addon"></span>
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">Brand</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input name="brand" placeholder="Brand" class="form-control" type="text" value="${perfume ? perfume.brand : ''}">
      <span class="input-group-addon"></span>
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">Volume</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input name="volume" placeholder="Volume" class="form-control" type="text"
             value="${perfume ? perfume.volume : ''}">
      <span class="input-group-addon">mL</span>
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">Price</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input name="price" placeholder="Price" class="form-control" type="text" value="${perfume ? perfume.price : ''}">
      <span class="input-group-addon">$</span>
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">Balance</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input name="balance" placeholder="Balance" class="form-control" type="text" value="${perfume ? perfume.balance : ''}">
      <span class="input-group-addon"></span>
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">External ID</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input name="externalId" placeholder="SKU" class="form-control" type="text"
             value="${perfume ? perfume.externalId : ''}">
      <span class="input-group-addon"></span>
    </div>
  </div>
</div>
