<!doctype html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>Perfume Storeroom</title>
</head>

<body>
<div class="container">
  <g:form action="create" method="post" controller="catalogue" class="well form-horizontal">
    <g:if test="${errors}">
      <h3 class="text-danger">Please fill all fields correctly</h3>
    </g:if>

    <br>

    <legend>New perfume</legend>
    <fieldset>
      <g:render template="perfume" model="${[perfume: perfume]}"/>

      <div class="form-group">
        <label class="col-md-4 control-label"></label>

        <div class="col-md-4">
          <button type="submit" class="btn btn-success">Create</button>
        </div>
      </div>

    </fieldset>
  </g:form>
</div>
</body>