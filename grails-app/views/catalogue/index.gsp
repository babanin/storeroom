<!doctype html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>Perfume Storeroom</title>
</head>

<body>

<br>
<g:form controller="catalogue" action="index">
  <div class="input-group">
    <span class="input-group-addon glyphicon glyphicon-search" id="basic-addon1"></span>
    <input type="text" class="form-control" placeholder="Search..." aria-describedby="basic-addon1"
           value="${search}" name="search">
    <span class="input-group-addon">
      <g:checkBox name="searchLowBalance" aria-label="less than 5 items in warehouse" value="${searchLowBalance}"
                  onChange="this.form.submit()"/> only low balance</span>
  </div>
</g:form>

<div class="row" style="padding: 20px 0 20px 0">
  <div class="col-md-2" style="float: left"><a href="${createLink(controller: "catalogue", action: "create")}"
                                               class="btn btn-success">New perfume</a></div>

  <div class="col-md-2 col-md-offset-8">
    <div class="dropdown">
      <button class="btn btn-info dropdown-toggle" type="button" id="exportMenu" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="true">
        Export table
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" aria-labelledby="exportMenu">
        <li><g:link controller="export" action="xls"
                    params="${[search: search, searchLowBalance: searchLowBalance]}">Excel (*.xls)</g:link></li>
      </ul>
    </div>
  </div>
</div>

<table class="table table-striped">
  <thead>
  <tr>
    <g:sortableColumn property="externalId" title="SKU"/>
    <g:sortableColumn property="name" title="Name"/>
    <g:sortableColumn property="brand" title="Brand"/>
    <g:sortableColumn property="balance" title="Balance"/>
    <g:sortableColumn property="volume" title="Volume"/>
    <g:sortableColumn property="price" title="Price"/>
    <th></th>
  </tr>
  </thead>

  <tbody>

  <g:each var="perfume" in="${perfumes}">
    <tr>
      <td>${perfume.externalId}</td>
      <td>${perfume.name}</td>
      <td>${perfume.brand}</td>
      <td>${perfume.balance}</td>
      <td>${perfume.volume}</td>
      <td>${perfume.price}</td>
      <td>
        <a href="${createLink(controller: "catalogue", action: "update", params: [id: perfume.id])}"
           class="btn btn-default perfume-action" aria-label="Left Align">
          <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        </a>

        <g:form controller="catalogue" action="delete">
          <input type="hidden" name="id" value="${perfume.id}"/>
          <button type="submit" class="btn btn-danger perfume-action" aria-label="Left Align">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </button>
        </g:form>
      </td>
    </tr>
  </g:each>
  </tbody>

  <tfoot>
  <tr>
    <td colspan="6">
      <b><g:if test="${search}">Search results</g:if> <g:else>Total</g:else></b>: ${perfumeCount}
    </td>

  </tr>
  </tfoot>
</table>

<div class="perfume-pagination">
  <g:paginate next="Forward" prev="Back" maxsteps="0"
              max="${grailsApplication.config.app.pagination}" controller="catalogue" action="index"
              total="${perfumeCount}" params="${[search: search]}"/>
</div>
</body>
</html>
