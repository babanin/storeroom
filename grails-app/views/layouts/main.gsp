<!doctype html>
<html>
<head>
  <title>
    <g:layoutTitle default="Perfume Storeroom"/>
  </title>

  <asset:stylesheet src="application.css"/>

  <g:layoutHead/>

  <asset:javascript src="application.js"/>

  <link rel="icon" type="image/png" href="${resource(dir: 'images', file: 'perfume.png')}" />
</head>

<body>
<div class="container">
  <div class="header clearfix">
    <nav>
      <ul class="nav nav-pills pull-right">
        <g:menuItem controller="catalogue" action="index">Catalogue</g:menuItem>
        <g:menuItem controller="import" action="index">Import</g:menuItem>
        <g:menuItem controller="user" action="index">Users</g:menuItem>
      </ul>
    </nav>

    <h3 class="text-muted"><img src="${resource(dir: 'images', file: 'perfume.png')}" alt="Logo"
                                border="0"/> Perfume Storeroom
    </h3>
  </div>

  <g:layoutBody/>

  <div class="footer">
  Whats up <i class="glyphicon glyphicon-user"></i> <sec:loggedInUserInfo field="username"/>? (<a href="/logoff">run away</a>)
  </div>
</div>
</body>
</html>
