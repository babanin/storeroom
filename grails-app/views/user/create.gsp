<!doctype html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>Perfume Storeroom - New user</title>
</head>

<body>
<div class="container">
  <br>

  <g:form action="create" method="post" controller="user" class="well form-horizontal">
    <g:if test="${errors}">
      <h3 class="text-danger">Please fill all fields correctly</h3>
    </g:if>

    <br>

    <legend>New user</legend>
    <fieldset>
      <g:render template="userForm" model="${[user: user]}"/>

      <div class="form-group">
        <label class="col-md-4 control-label"></label>

        <div class="col-md-4">
          <button type="submit" class="btn btn-success">Create</button>
        </div>
      </div>
    </fieldset>
  </g:form>
</div>
</body>