<!doctype html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>Perfume Storeroom - Users</title>
</head>

<body>

<br>

<div class="row" style="padding: 0 0 20px 0">
  <div class="col-md-2" style="float: left"><a href="${createLink(controller: "user", action: "create")}"
                                               class="btn btn-success">New user</a></div>
</div>

<table class="table table-striped" id="usersTable">
  <thead>
  <tr>
    <th>Username</th>
    <th>Administrator</th>
    <th>Read only</th>
    <th></th>
  </tr>
  </thead>
  <tbody>
  <g:each in="${users}" var="user">
    <g:set var="readOnly" value="${user.roles.find { it.name == 'ROLE_READ_ONLY' }}"/>
    <tr>
      <td>${user.username}</td>
      <td><g:if test="${!readOnly}"><img src="${resource(dir: 'images', file: 'checked_user.png')}"/></g:if></td>
      <td><g:if test="${readOnly}"><img src="${resource(dir: 'images', file: 'checked_user.png')}"/></g:if></td>
      <td>
        <a href="${createLink(controller: "user", action: "update", params: [id: user.id])}"
           class="btn btn-default perfume-action" aria-label="Left Align">
          <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        </a>

        <g:form controller="user" action="delete">
          <input type="hidden" name="id" value="${user.id}"/>
          <button type="submit" class="btn btn-danger perfume-action" aria-label="Left Align">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </button>
        </g:form>
      </td>
    </tr>
  </g:each>
  </tbody>
</table>
</body>
</html>
