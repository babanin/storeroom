<div class="form-group">
  <label class="col-md-4 control-label">Username</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input name="username" placeholder="Name" class="form-control" type="text" value="${user ? user.username : ''}">
      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">Password</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input id="password" type="password" class="form-control" name="password" placeholder="Password">
      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">Read only</label>

  <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
      <input id="readOnly" type="checkbox" class="form-control" name="readOnly">
    </div>
  </div>
</div>
