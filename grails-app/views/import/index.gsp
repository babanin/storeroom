<!doctype html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>Perfume Storeroom</title>

</head>

<body>

<g:javascript>
  $(document).on('click', '.browse', function () {
    var file = $(this).parent().parent().parent().find('.file');
    file.trigger('click');
  });

  $(document).on('change', '.file', function () {
    $(this).parent().find('.form-control').val($(this).val());
  });
</g:javascript>


<div class="container">
  <g:uploadForm action="upload" style="display: inline">
    <h3>Select file to import</h3>
    <br>

    <div class="form-group">
      <input type="file" name="reception" class="file">

      <div class="input-group col-xs-12">
        <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>

        <input type="text" class="form-control input-lg" disabled
               placeholder="Select file with perfumes (*.xls, *.csv)">

        <span class="input-group-btn">
          <button class="browse btn btn-primary input-lg" type="button"><i
              class="glyphicon glyphicon-search"></i> Browse</button>
        </span>

        <span></span>

        <span class="input-group-btn">
          <input type="submit" class="btn btn-success input-lg" value="Upload"/>
        </span>
      </div>
    </div>
  </g:uploadForm>

</div>
</body>
</html>
