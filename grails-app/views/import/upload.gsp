<!doctype html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>Perfume Storeroom</title>
</head>

<body>

<div class="container">
  <g:if test="${error}">
    <h3 class="text-danger">Error while processing <b>${fileName}</b></h3>
    <br>
    <h4 class="text-danger">${error}</h4>
  </g:if>
  <g:else>
    <h3 class="text-info">File <b>${fileName}</b> processed</h3>

    <br>
    <h4>Rows
      <span class="text-success">imported: ${result.imported}</span> / <span
        class="text-danger">skipped: ${result.skipped}</span>
    </h4>
    <br>

    <g:if test="${result.notes}">
      <table class="table table-striped">
        <thead>
        <tr>
          <td colspan="3" style="width: 50%"><h4>Entry</h4></td>
          <td rowspan="2"><h4>Error</h4></td>
        </tr>
        <tr>
          <td>SKU</td>
          <td>Name</td>
          <td>Balance</td>
          <td></td>
        </tr>
        </thead>
        <tbody>
        <g:each var="note" in="${result.notes}">
          <tr class="${note.type}">
            <td>${note.entry.sku}</td>
            <td>${note.entry.name}</td>
            <td>${note.entry.balance}</td>
            <td class="text-${note.type.toString().toLowerCase()}">${note.message}</td>
          </tr>
        </g:each>
        </tbody>
      </table>
    </g:if>
  </g:else>
</div>
</body>
</html>