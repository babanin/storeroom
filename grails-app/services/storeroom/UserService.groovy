package storeroom

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ib.storeroom.auth.Role
import ib.storeroom.auth.User
import ib.storeroom.commands.DeleteUserCommand
import ib.storeroom.commands.NewUserCommand
import ib.storeroom.commands.UserDetailsCommand

@Transactional
class UserService {

  @Secured('ROLE_ADMIN')
  def create(NewUserCommand command) {
    User user = new User(
        username: command.username,
        password: command.password,
        roles: [Role.findByName(command.readOnly ? 'ROLE_READ_ONLY' : 'ROLE_ADMIN')]
    )

    user.save(failOnError: true)
  }

  @Secured('ROLE_ADMIN')
  def update(UserDetailsCommand command) {
    def user = User.get(command.id)

    user.username = command.username
    user.password = command.password
    user.roles = [Role.findByName(command.readOnly ? 'ROLE_READ_ONLY' : 'ROLE_ADMIN')]

    user.save(failOnError: true)
  }

  @Secured('ROLE_ADMIN')
  def delete(DeleteUserCommand command) {
    final user = User.get(command.id)
    user.delete(failOnError: true)
  }
}
