package storeroom

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ib.storeroom.Perfume
import ib.storeroom.commands.EditPerfumeCommand
import ib.storeroom.commands.NewPerfumeCommand
import org.codehaus.groovy.runtime.InvokerHelper

@Transactional
class CatalogueService {
  @Secured('ROLE_ADMIN')
  def create(NewPerfumeCommand command) {
    final perfume = new Perfume(command.properties)
    perfume.save()
  }

  @Secured('ROLE_ADMIN')
  def update(EditPerfumeCommand command) {
    final perfume = Perfume.get(command.id)
    use(InvokerHelper) {
      perfume.setProperties(command.properties)
    }

    perfume.save(failOnError: true)
  }

  @Secured('ROLE_ADMIN')
  def delete(id) {
    final perfume = Perfume.get(id)
    perfume.delete()
  }
}
