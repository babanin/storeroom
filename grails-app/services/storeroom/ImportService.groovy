package storeroom

import com.google.common.collect.ImmutableList
import grails.transaction.Transactional
import ib.storeroom.Perfume
import ib.storeroom.fileimport.ImportResult
import ib.storeroom.fileimport.Note
import ib.storeroom.fileimport.NoteType
import ib.storeroom.fileimport.RawEntry

@Transactional
class ImportService {
  static final VALIDATION_PIPELINE = [
      new NullPropertyValidation("sku"),
      new NullPropertyValidation("name"),
      new NullPropertyValidation("balance"),

      [
          isValid : { e -> (e.sku instanceof Number) || (e.sku instanceof String && e.sku.isLong()) },
          message : { e -> "Invalid SKU value: ${e.sku}" },
          severity: { NoteType.DANGER }
      ] as RawEntryValidator,

      [
          isValid : { e -> (e.sku as Long) > 0 },
          message : { e -> "SKU must be positive value: ${e.sku}" },
          severity: { NoteType.DANGER }
      ] as RawEntryValidator,

      [
          isValid : { e -> (e.balance instanceof Number) || (e.balance instanceof String && e.balance.isLong()) },
          message : { e -> "Invalid balance value: ${e.balance}" },
          severity: { NoteType.DANGER }
      ] as RawEntryValidator,

      [
          isValid : { e -> (e.balance as Long) > 0 },
          message : { e -> "Balance must be positive value: ${e.balance}" },
          severity: { NoteType.DANGER }
      ] as RawEntryValidator,

      [
          isValid : { e -> Perfume.get(e.sku.toLong()) != null },
          message : { e -> "Unable to find perfume with external ID: ${e.sku.toLong()}" },
          severity: { NoteType.DANGER }
      ] as RawEntryValidator
  ]

  ImportResult processEntries(List<RawEntry> rawEntries) {
    def imported = 0
    def skipped = 0
    final notesBuilder = ImmutableList.builder()

    for (RawEntry rawEntry : rawEntries) {
      final failed = VALIDATION_PIPELINE.find { !it.isValid(rawEntry) }
      if (failed) {
        notesBuilder.add(new Note(rawEntry, failed.message(rawEntry), failed.severity()))
        skipped++
        continue
      }

      final externalId = rawEntry.sku as Long
      final Perfume perfume = Perfume.get(externalId)

      if (perfume.name != rawEntry.name) {
        notesBuilder.add(new Note(rawEntry, "Perfume with SKU = ${externalId} has different name: ${rawEntry.name} ≠ ${perfume.name}", NoteType.WARNING))
      }

      perfume.balance += rawEntry.balance
      perfume.save()

      imported++
    }

    new ImportResult(imported, skipped, notesBuilder.build())
  }

  static interface RawEntryValidator {
    boolean isValid(RawEntry entry)

    def message(RawEntry entry)

    NoteType severity()
  }

  static class NullPropertyValidation implements RawEntryValidator {
    String property

    NullPropertyValidation(String property) {
      this.property = property
    }

    @Override
    boolean isValid(RawEntry entry) { entry."$property" }

    @Override
    String message(RawEntry entry) { "$property can not be empty value" }

    @Override
    NoteType severity() { NoteType.DANGER }
  }
}
