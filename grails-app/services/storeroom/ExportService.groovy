package storeroom

import grails.transaction.Transactional
import ib.storeroom.Perfume
import ib.storeroom.commands.SearchPerfumeCommand
import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Workbook

@Transactional
class ExportService {
  def values(command) {
    Perfume.createCriteria().list() {
      and {
        if (command.search) {
          or {
            ilike("brand", "%${command.search}%")
            ilike("name", "%${command.search}%")
          }
        }

        if (command.searchLowBalance) {
          lt("balance", 5)
        }
      }
    }
  }
}
