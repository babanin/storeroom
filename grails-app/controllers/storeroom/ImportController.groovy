package storeroom

import grails.plugin.springsecurity.annotation.Secured
import ib.storeroom.fileimport.ImportFileParser

@Secured('ROLE_ADMIN')
class ImportController {
  def importService

  def index() {
  }

  def upload() {
    final receptionFile = request.getFile('reception')

    if (!receptionFile.isEmpty()) {
      final filename = receptionFile.filename
      final extension = filename.drop(filename.lastIndexOf('.') + 1)

      final parser = ImportFileParser.REGISTRY[extension]
      if (!parser) {
        return render(view: "upload", model: [fileName: filename, error: "Unsupported file extension: ${extension}"])
      }

      try {
        final rawEntries = parser.parse(receptionFile)
        final importResult = importService.processEntries(rawEntries)
        render(view: "upload", model: [fileName: filename, result: importResult])
      } catch (e) {
        render(view: "upload", model: [fileName: filename, error: e.message])
      }
    } else {
      render(view: "index")
    }
  }
}
