package storeroom

import grails.plugin.springsecurity.annotation.Secured
import ib.storeroom.Perfume
import ib.storeroom.commands.EditPerfumeCommand
import ib.storeroom.commands.NewPerfumeCommand
import org.codehaus.groovy.runtime.InvokerHelper

class CatalogueController {
  def catalogueService

  @Secured(['ROLE_ADMIN', 'ROLE_READ_ONLY'])
  def index() {
    if (!params.offset) {
      params.offset = 0
    }

    if (!params.max) {
      params.max = grailsApplication.config.app.pagination
    }

    final searchString = params.search
    final searchLowBalance = params.searchLowBalance

    final pattern = "%$searchString%"
    final perfumes = Perfume.createCriteria().list(params) {
      and {
        if (searchString) {
          or {
            ilike("brand", pattern)
            ilike("name", pattern)
          }
        }

        if (searchLowBalance) {
          lt("balance", 5)
        }
      }
    }

    final perfumeCount = Perfume.createCriteria().count {
      and {
        if (searchString) {
          or {
            ilike("brand", pattern)
            ilike("name", pattern)
          }
        }

        if (searchLowBalance) {
          lt("balance", 5)
        }
      }
    }

    [
        perfumes        : perfumes,
        perfumeCount    : perfumeCount,
        search          : searchString,
        searchLowBalance: searchLowBalance
    ]
  }

  @Secured('ROLE_ADMIN')
  def create(NewPerfumeCommand command) {
    if (request.method == 'GET') {
      return render(view: "create")
    }

    final perfume = catalogueService.create(command)
    if (perfume.hasErrors()) {
      return render(view: "create", model: [perfume: perfume, errors: perfume.errors])
    }

    redirect(controller: "catalogue", action: "index")
  }

  @Secured('ROLE_ADMIN')
  def update(EditPerfumeCommand command) {
    final perfume = Perfume.get(command.id)

    if (request.method == 'GET') {
      return render(view: "update", model: [perfume: perfume])
    }

    final updatedPerfume = catalogueService.update(command)
    if (updatedPerfume.hasErrors()) {
      return render(view: "update", model: [perfume: perfume, errors: perfume.errors])
    }

    redirect(controller: "catalogue", action: "index")
  }

  @Secured('ROLE_ADMIN')
  def delete() {
    final id = params.id

    if (id) {
      catalogueService.delete(id)
    }

    redirect(controller: "catalogue", action: "index")
  }
}
