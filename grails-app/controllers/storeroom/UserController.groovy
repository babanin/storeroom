package storeroom

import grails.plugin.springsecurity.annotation.Secured
import ib.storeroom.auth.Role
import ib.storeroom.auth.User
import ib.storeroom.commands.DeleteUserCommand
import ib.storeroom.commands.NewUserCommand
import ib.storeroom.commands.UserDetailsCommand

@Secured('ROLE_ADMIN')
class UserController {
  def userService

  def index() {
    [users: User.list()]
  }

  def create(NewUserCommand command) {
    if (request.method == 'GET') {
      return render(view: 'create')
    }

    final user = userService.create(command)
    if (user.hasErrors()) {
      return render(view: 'create', model: [user: user, errors: user.errors])
    }

    redirect(contoller: 'user', action: 'index')
  }

  def update(UserDetailsCommand command) {
    if (request.method == 'GET') {
      final user = User.get(command.id)
      return render(view: 'update', model: [user: user])
    }

    userService.update(command)
    redirect(contoller: 'user', action: 'index')
  }

  def delete(DeleteUserCommand command) {
    userService.delete(command)
    redirect(contoller: 'user', action: 'index')
  }
}
