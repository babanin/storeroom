package storeroom

import grails.plugin.springsecurity.annotation.Secured
import ib.storeroom.Perfume
import ib.storeroom.commands.SearchPerfumeCommand
import ib.storeroom.fileexport.FileUtils
import ib.storeroom.fileexport.POIUtils
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Font

class ExportController {
  def exportService

  @Secured(["ROLE_ADMIN", "ROLE_READ_ONLY"])
  def xls(SearchPerfumeCommand command) {
    final outputFile = new HSSFWorkbook();
    final sheet = outputFile.createSheet("Perfumes")

    sheet.createRow(0).with {
      CellStyle style = outputFile.createCellStyle();
      Font font = outputFile.createFont();
      font.setBold(true);
      style.setFont(font);

      ["SKU", "Name", "Brand", "Volume", "Price", "Balance"].eachWithIndex { String colHeader, int column ->
        createCell(column).with {
          setCellStyle(style)
          setCellValue(colHeader)
        }
      }
    }

    exportService.values(command).eachWithIndex { Perfume perfume, int index ->
      use(POIUtils) {
        perfume.toRow(sheet.createRow(index + 1))
      }
    }

    response.contentType = 'application/vnd.ms-excel'
    response.addHeader("Content-Disposition", "attachment;filename=${FileUtils.generateFileName(command)}.xls")

    outputFile.write(response.outputStream)
    outputFile.close()

    response.outputStream.flush()
  }
}
