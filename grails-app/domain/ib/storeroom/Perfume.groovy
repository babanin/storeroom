package ib.storeroom

class Perfume {
  long externalId

  String name
  String brand

  int balance

  int volume
  double price

  static constraints = {
    externalId unique: true
    name blank: false
    brand blank: false
    balance min: 0
    volume min: 1
    price min: (0.01).toDouble()
  }
}
