package ib.storeroom.auth

class Role {
  String name

  static constraints = {
    name blank: false, unique: true
  }
}
