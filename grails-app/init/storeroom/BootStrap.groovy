package storeroom

import com.xlson.groovycsv.CsvParser
import ib.storeroom.Perfume
import ib.storeroom.auth.Role
import ib.storeroom.auth.User

class BootStrap {
  def sessionFactory

  def init = { servletContext ->
    users()
    perfumes()

    sessionFactory.currentSession.flush()
  }

  private void perfumes() {
    CsvParser.parseCsv([columnNames: ['name', 'brand']], new FileReader("test-data/wiki-brands.csv")).eachWithIndex { it, index ->
      new Perfume(externalId: index, name: it.name, brand: it.brand, balance: index, volume: index + 10, price: index / 100 + 0.1).save(failOnError: true)
    }
  }

  private void users() {
    def adminAuth = new Role(name: 'ROLE_ADMIN').save()
    def readOnlyAuth = new Role(name: 'ROLE_READ_ONLY').save()

    def guest = new User(username: 'guest', password: 'password', roles: [readOnlyAuth]).save()
    def admin = new User(username: 'admin', password: 'password', roles: [adminAuth]).save()

    assert User.count() == 2
    assert Role.count() == 2
  }

  def destroy = {
  }
}
