package ib.storeroom.fileexport

import ib.storeroom.Perfume
import org.apache.poi.ss.usermodel.Row

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
final class POIUtils {
  static toRow(Perfume perfume, Row row) {
    row.with {
      def index = 0

      createCell(index++).setCellValue(perfume.externalId)
      createCell(index++).setCellValue(perfume.name)
      createCell(index++).setCellValue(perfume.brand)
      createCell(index++).setCellValue(perfume.volume)
      createCell(index++).setCellValue(perfume.price)
      createCell(index++).setCellValue(perfume.balance)
    }
  }
}
