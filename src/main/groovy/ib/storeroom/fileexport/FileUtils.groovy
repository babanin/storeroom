package ib.storeroom.fileexport

import ib.storeroom.commands.SearchPerfumeCommand

import java.text.SimpleDateFormat

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
final class FileUtils {
  static String generateFileName(SearchPerfumeCommand command) {
    final search = command.search ? command.search.toLowerCase().replaceAll("[^a-zA-Z0-9-_\\.]", "_") : "all_perfume"
    final lowBalance = command.searchLowBalance ? '_low_balance' : ''
    final date = new SimpleDateFormat("__HH_mm__dd_MM_yyyy").format(new Date())

    search + lowBalance + date
  }
}
