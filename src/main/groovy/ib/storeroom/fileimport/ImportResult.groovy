package ib.storeroom.fileimport

import groovy.transform.Immutable

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
final class ImportResult {
  final imported
  final skipped
  final notes

  ImportResult(imported, skipped, notes) {
    this.imported = imported
    this.skipped = skipped
    this.notes = notes
  }
}

final class Note {
  final RawEntry entry
  final String message
  final NoteType type

  Note(RawEntry entry, String message, NoteType type) {
    this.entry = entry
    this.message = message
    this.type = type
  }
}

enum NoteType {
  WARNING, DANGER
}
