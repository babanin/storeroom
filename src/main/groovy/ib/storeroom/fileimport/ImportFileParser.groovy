package ib.storeroom.fileimport

import com.xlson.groovycsv.CsvParser
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.springframework.web.multipart.MultipartFile

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
interface ImportFileParser {
  List<RawEntry> parse(MultipartFile file)

  def REGISTRY = [csv: new CSVParser(), xls: new XlsParser()]
}

final class CSVParser implements ImportFileParser {
  @Override
  List<RawEntry> parse(MultipartFile file) {
    CsvParser.parseCsv([columnNames: ['sku', 'name', 'balance']], new InputStreamReader(file.inputStream)).collect {
      new RawEntry(it.properties)
    }
  }
}

final class XlsParser implements ImportFileParser {
  @Override
  List<RawEntry> parse(MultipartFile file) {
    new HSSFWorkbook(file.getInputStream()).getSheetAt(0).collect { readEntry(it) }
  }

  private static RawEntry readEntry(Row row) {
    final values = []
    for (int i = 0; i < 3; i++) {
      values << value(row.getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL))
    }

    values as RawEntry
  }

  private static value(Cell cell) {
    switch (cell.cellTypeEnum) {
      case CellType.BOOLEAN:
        return cell.booleanCellValue
      case CellType.NUMERIC:
        return cell.numericCellValue
      case CellType.STRING:
        return cell.stringCellValue
      case CellType.BLANK:
      case CellType.ERROR:
      case CellType.FORMULA:
        return null
    }
  }
}
