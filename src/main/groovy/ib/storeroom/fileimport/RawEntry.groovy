package ib.storeroom.fileimport
/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
final class RawEntry {
  final sku
  final name
  final balance

  RawEntry(sku, name, balance) {
    this.sku = sku
    this.name = name
    this.balance = balance
  }
}
