package ib.storeroom.commands

import grails.validation.Validateable

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
abstract class UserCommand implements Validateable {
  String username
  String password
  boolean readOnly
}

final class NewUserCommand extends UserCommand implements CommandWithId {}

final class UserDetailsCommand extends UserCommand implements CommandWithId {}

final class DeleteUserCommand implements CommandWithId {}