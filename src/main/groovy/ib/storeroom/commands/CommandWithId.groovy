package ib.storeroom.commands

import grails.validation.Validateable

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
trait CommandWithId<T> implements Validateable {
  long id

//  static constraints = {
//    id validator: { val, obj -> true }
//  }
}
