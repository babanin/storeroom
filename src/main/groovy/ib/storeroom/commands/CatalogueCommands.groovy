package ib.storeroom.commands

import grails.validation.Validateable
import ib.storeroom.Perfume

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
abstract class PerfumeCommand implements Validateable {
  String name
  String brand

  long externalId

  int balance
  int volume
  double price
}

final class NewPerfumeCommand extends PerfumeCommand {}

final class EditPerfumeCommand extends PerfumeCommand implements CommandWithId<Perfume> {}

final class DeletePerfumeCommand implements CommandWithId<Perfume> {}

final class SearchPerfumeCommand implements Validateable {
  String search
  boolean searchLowBalance
}